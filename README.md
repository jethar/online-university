Online University
================

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

**Online Univeristy** is application for education institution. Students enroll for various courses. Each course has a defined syllabus and chapters. Professors teach the students as per the syllabus of the course.


## Features

The following features are supported :

1. As a site visitor, can register on site.
1. As a admin can change role of user to professor.
1. As a profressor can create course.
1. As a Profressor can edit own course.
1. As a student can view course.
1. As a professor, can add categories to course.
1. As a admin / profressor, can invite other professors to register.
1. As a student, can enroll in course

Following are the future features which are leading from above ones.

* As a developer, allow for configuration which is not in git for security reasons.
* As a deployer, deploy to Heroku
* As a student when first enter, or when have no courses, is prompted to select and enroll in course
* As a new professor, given information about the features and prompted to create a new course
* As a developer ensure, course can have multiple categories
* As a developer, provide support for markdown syntax in description and syllabus
* As a student I get toast messages on course updates.
* As a student can search for courses
* As a professor, can provide re-requisites for the course (on the application)
  - As a professor, can provide re-requisites for the course which are external
* As a Professor, add other profressor to course
* As a professor can create message categories which go as toast message to students

## Supported Roles

Following roles are available -

1. Admin
2. Professor
3. Student
4. Anonymous Site Visitor

## Deployment Information

The confirguration of application is supported through `config/secrets.yml`. Additionally environment variables are supported via the `config/local_env.yml`.

```
# Each entry gets set as a local environment variable.
# This file overrides ENV variables in the Unix shell.
# For example, setting:
# GMAIL_USERNAME: 'Your_Gmail_Username'
```

### Environment Variables

The application supports following environment variable -

* GMAIL_USERNAME
* GMAIL_PASSWORD
* DB_HOST
* DB_USERNAME
* DB_PASSWORD

In the above assumption is that postgres database is used. This can be changed in `config/database.yml`.

### Commands

Run the following steps for simple development deployment (One can also use environments like rbenv or rvm w/gemsets to isolate environment) -

```
bundle install
# or bundle install --without production
rake db:setup
rails s
```
