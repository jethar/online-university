class Category < ActiveRecord::Base

  has_many :categorize
  has_many :courses, through: :categorize

end
