class Course < ActiveRecord::Base

  has_many :categorize
  has_many :categories, through: :categorize
  has_and_belongs_to_many :users,  :join_table => :users_courses

  validates_presence_of :name, :code, :start_date, :end_date

  before_save :sanitize_content

  def all_categories=(names)
    self.categories = names.split(",").map do |name|
        Category.where(name: name.downcase.strip.gsub(/\s+/, "-")).first_or_create!
    end
  end

  def all_categories
    self.categories.map(&:name).join(", ")
  end

  def self.categorized(name)
    Category.find_by_name!(name).courses
  end

  def sanitize_content
    self.code = self.code.gsub(/\s+/, "").downcase
  end

  def enrolled?(current_user)
    return false if current_user.nil? || !current_user.student?
    self.users.find_by_id(current_user.id).present?
  end

  def can_enroll?(current_user)
    (current_user.nil? || current_user.student?) ? true : false
  end

end
