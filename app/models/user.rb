class User < ActiveRecord::Base
  enum role: [:student, :professor, :admin]
  after_initialize :set_default_role, :if => :new_record?

  has_and_belongs_to_many :courses,  :join_table => :users_courses

  validates_presence_of :email, :role

  def set_default_role
    self.role ||= :student
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,# :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
end
