class CoursePolicy  < ApplicationPolicy

  class Scope < Scope

    def resolve
      if user && (user.professor? || user.student?)
        scope.joins(:users).where(["users.id = ?", user.id])
        # User.find(@user).posts
      else
        scope.all
      end
    end
  end

  def create?
    user && (user.admin? || user.professor?)
  end

  def update?
    user && (user.admin? || user.professor?)
  end

  def destroy?
    user && (user.admin? || user.professor?)
  end

  def enroll?
    user && user.student? && @record.present?
  end

  def enrolled?
    return false if user.nil? || !user.student?
    @record.users.find_by_id(user.id).present?
  end

end
