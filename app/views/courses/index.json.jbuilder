json.array!(@courses) do |course|
  json.extract! course, :id, :name, :code, :description, :start_date, :end_date, :duration, :grade_point, :syllabus
  json.categories course.categories, :name
  json.url course_url(course, format: :json)
end
