json.extract! @course, :id, :name, :code, :description, :start_date, :end_date, :duration, :grade_point, :syllabus, :created_at, :updated_at
json.categories @course.categories, :name
