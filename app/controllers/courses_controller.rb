class CoursesController < ApplicationController
  respond_to :html, :json
  before_action :set_course, only: [:show, :edit, :update, :destroy, :enroll]

  before_filter :authenticate_user!, except: [:index, :index_all, :show]

  def index
    if params[:category]
      @courses = Course.categorized(params[:category])
    else
      @courses = policy_scope(Course)
    end
    respond_with(@courses)
  end

  def index_all
    @courses = Course.all
    respond_with(@courses, template: 'courses/index')
  end

  def show
    respond_with(@course)
  end

  def new
    @course = Course.new
    authorize @course
    respond_with(@course)
  end

  def edit
    authorize @course
  end

  def create
    @course = Course.new(course_params)
    @course.users << User.find(current_user.id)
    authorize @course
    flash[:notice] = 'Course was successfully created.' if @course.save
    respond_with(@course)
  end

  def update
    authorize @course
    flash[:notice] = 'Course was successfully updated.' if @course.update(course_params)
    respond_with(@course)
  end

  def destroy
    authorize @course
    @course.destroy
    respond_with(@course)
  end

  def enroll
    @course.users << current_user
    @course.save
  end

  private
    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:name, :code, :description, :start_date, :end_date, :duration, :grade_point, :syllabus, :all_categories)
    end
end
