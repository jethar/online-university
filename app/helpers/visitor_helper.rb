module VisitorHelper

  def admin_email()
    User.where("role = ?", User.roles[:admin]).first.email rescue 'No Admin'
  end

end
