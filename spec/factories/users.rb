FactoryGirl.define do
  factory :user do
    confirmed_at Time.now
    name "Test User"
    email "test@example.com"
    password "please123"

    trait :admin do
      role 'admin'
    end

    trait :professor do
      role 'professor'
    end

    trait :student do
      role 'student'
    end
  end
end
