FactoryGirl.define do
  factory :course do
    sequence(:name) {|n| "MyString_#{n}"}
    sequence(:code) {|n| "MyString_#{n}"}
    description "MyText"
    start_date "2014-10-31"
    end_date "2014-10-31"
    duration 1
    grade_point 1
    syllabus "MyText"
  end

end
