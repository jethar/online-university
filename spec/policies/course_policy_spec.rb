describe CoursePolicy do
  subject { CoursePolicy }

  let (:course) { FactoryGirl.build_stubbed :course }

  let (:current_user) { FactoryGirl.build_stubbed :user, :current_user }
  let (:admin) { FactoryGirl.build_stubbed :user, :admin }
  let (:professor) { FactoryGirl.build_stubbed :user, :professor }
  let (:other_user) { FactoryGirl.build_stubbed :user }
  let (:student) { FactoryGirl.build_stubbed :user }

  permissions :create? do
    it "prevents create if other_user" do
      expect(subject).not_to permit(other_user, course)
    end
    it "allows an admin/professor to make course" do
      expect(subject).to permit(admin, course)
      expect(subject).to permit(professor, course)
    end
  end

  permissions :update? do
    it "prevents updates if other_user" do
      expect(subject).not_to permit(other_user, course)
    end
    it "allows an admin to make updates" do
      expect(subject).to permit(admin, course)
    end
  end

  permissions :destroy? do
    it "prevents deleting if not admin/professor" do
      expect(subject).not_to permit(other_user, course)
    end
    it "allows an admin/professor to delete any course" do
      expect(subject).to permit(admin, course)
      expect(subject).to permit(professor, course)
    end
  end

  permissions :enroll? do
    it "prevents create if no course or admin" do
      expect(subject).not_to permit(student, nil)
      expect(subject).not_to permit(admin, course)
    end

    it "allows student to enroll course" do
      expect(subject).to permit(student, course)
    end

    it "not allows professor to enroll course" do
      expect(subject).not_to permit(professor, course)
    end

  end
end
