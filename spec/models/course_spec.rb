require 'rails_helper'

RSpec.describe Course, :type => :model do
  describe "Associations" do
    it { should have_many(:categorize)}
    it { should have_many(:categories)}
    it { should have_and_belong_to_many(:users)}
  end

  describe "all_categories=" do
    it "should set or create categories" do
      course = FactoryGirl.create(:course)
      category1 = FactoryGirl.create(:category, :name => "maths")
      category2 = FactoryGirl.create(:category, :name => "science")
      FactoryGirl.create(:categorize, :course => course, :category => category1)
      FactoryGirl.create(:categorize, :course => course, :category => category2)

      Category.count.should == 2
      course.all_categories = "maths, science, art"
      Category.count.should == 3
      course.all_categories.should == "maths, science, art"
    end
  end

  describe "all_categories" do
    it "should return list of all categories" do
      course = FactoryGirl.create(:course)
      category1 = FactoryGirl.create(:category, :name => "maths")
      category2 = FactoryGirl.create(:category, :name => "science")
      FactoryGirl.create(:categorize, :course => course, :category => category1)
      FactoryGirl.create(:categorize, :course => course, :category => category2)

      course.all_categories.should == "maths, science"
    end
  end

  describe "categorized" do
    it "should return list of all categories with that name" do
      course = FactoryGirl.create(:course)
      category1 = FactoryGirl.create(:category, :name => "maths")
      category2 = FactoryGirl.create(:category, :name => "science")
      FactoryGirl.create(:categorize, :course => FactoryGirl.create(:course, :name => "one"), :category => category1)
      FactoryGirl.create(:categorize, :course => FactoryGirl.create(:course, :name => "two"), :category => category1)
      FactoryGirl.create(:categorize, :course => course, :category => category2)

      courses = Course.categorized("maths")
      courses.size.should == 2
      courses.collect(&:name).should == ["one", "two"]
    end
  end
end
