require 'rails_helper'

RSpec.describe Categorize, :type => :model do
  describe "Associations" do
    it { should belong_to(:course)}
    it { should belong_to(:category)}
  end
end
