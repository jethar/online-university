describe User do

  before(:each) { @user = User.new(email: 'user@example.com') }

  subject { @user }

  it { should respond_to(:email) }

  it "#email returns a string" do
    expect(@user.email).to match 'user@example.com'
  end

  describe "Associations" do
    it { should have_and_belong_to_many(:courses)}
  end

  describe "Validations User Creation" do
    # it { should validate_presence_of(:name).with_message(/#{I18n.t("activerecord.errors.models.user.blank")}/) }
    it { should validate_presence_of(:email).with_message(/#{I18n.t("activerecord.errors.models.user.blank")}/) }
    it { should validate_presence_of(:role).with_message(/#{I18n.t("activerecord.errors.models.user.blank")}/) }
  end


end
