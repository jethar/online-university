class CreateUsersCourses < ActiveRecord::Migration
  def change
    # create_table :users_courses do |t|
    #   t.belongs_to :course, index: true
    #   t.belongs_to :user, index: true

    #   t.timestamps
    # end

    create_table :users_courses, :id => false do |t|
      t.references :user, :null => false
      t.references :course, :null => false
    end

    # Adding the index can massively speed up join tables. Don't use the
    # unique if you allow duplicates.
    add_index(:users_courses, [:user_id, :course_id], :unique => true)

  end
end
