class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :name
      t.string :code
      t.text :description
      t.date :start_date
      t.date :end_date
      t.integer :duration
      t.integer :grade_point
      t.text :syllabus

      t.timestamps
    end
  end
end
